angular.module('leitor.controllers', ['ionic', 'ngCordova'])
	.controller("leitorController", function($scope, $cordovaBarcodeScanner, $cordovaSQLite){
		$scope.lerCodigo = function(){
			alert('Click funcionando');
			$cordovaBarcodeScanner.scan().then ( function(imagemEscaneada){
				alert(imagemEscaneada.text);

			}, function(error){
				alert("Ocorreu um erro: "+ error);

			});
		}

		$scope.resultado = "";
		$scope.productList = [];

		var db = null;
		
		$scope.insert = function(nome){
			$scope.productList = [];
			var query = "insert into produtos(nome) values(?)";
			$cordovaSQLite.execute(db,query,[nome]).then(function(result){
				$scope.resultado = "Insert OK";
			}, function(error){
				$scope.resultado = "Insert Fail";
			});
		}

		$scope.selectAll = function(){
			console.log($scope.fsName);
			$scope.productList = [];
			var query = "select nome from produtos";
			$cordovaSQLite.execute(db,query,[]).then(function(result){
				if(result.rows.length > 0){
					for (var i = 0; i < result.rows.length; i++) {
						$scope.productList.push({nome: result.rows.item(i).nome});
					}
					$scope.resultado = result.rows.length + "rows found";
				} else {
					$scope.resultado = "0 rows found";
					$scope.productList = [];
				}

			}, function(error){
				console.log(error);
			});
			$scope.fsName = "";
		}

		$scope.delete = function(nome){
			$scope.productList = [];
			var query = "delete from produtos where nome = ?";
			$cordovaSQLite.execute(db, query,[nome]).then(function(result){
				$scope.resultado = "Delete OK";
			}, function(error){
				$scope.resultado = "Delete Fail";
			});
		}

		$scope.edit = function(nome){
			$scope.productList = [];
			console.log("Vou fazer update");
			var query = "edit produtos set nome = ? where nome = ?";
			$cordovaSQLite.execute(db, query,[nome]).then(function(result){
				$scope.resultado = "Editado";
			}, function(error){
				$scope.resultado = "Edit Fail";
			});
		}
	});